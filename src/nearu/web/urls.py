from django.contrib import admin
from django.urls import path,re_path
from web import views


app_name="web"

urlpatterns = [
    path('',views.index),
    re_path(r'^product/(?P<pk>.*)/$',views.product, name="product"),
    re_path(r'^contact/$',views.contact, name="contact"),
]
