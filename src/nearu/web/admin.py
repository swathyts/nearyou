from django.contrib import admin
from products.models import Product, Category
from web.models import Contact


class ContactAdmin(admin.ModelAdmin):
    list_display = ('name','email','phone','message')
    search_fields = ('name',)


admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Contact, ContactAdmin)