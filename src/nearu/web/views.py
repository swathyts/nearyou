from django.shortcuts import render, get_object_or_404
from products.models import Category, Product
from web.models import Contact
from django.http import HttpResponse
from web.forms import ContactForm


def index(request):
    categories = Category.objects.all()
    products = Product.objects.all()
    latest_product = None

    if products:
        latest_product = products.latest('id')
    # print(latest_product)

    form = ContactForm()

    context = {
        "products" : products,
        "categories" : categories,
        "latest_product" : latest_product,
        "form" : form
    }

    return render(request, "index.html", context=context)


def product(request, pk):
    instance = get_object_or_404(Product, pk=pk)
    print(instance)

    categories = Category.objects.all()
    products = Product.objects.all()

    context = {
        "categories" : categories,
        "products" : products,
        "instance" : instance
    }
    
    return render(request,"productview.html", context=context)


def contact(request):
    # print(request.method)

    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
        else:
            pass

    return HttpResponse("Contact")