$(document).ready(function() {
	localStorage.setItem("isLoggedIn", false);
	if(localStorage.getItem("cart") === null){
		localStorage.setItem("cart", JSON.stringify({}))
	} else {
		console.log(JSON.parse(localStorage.getItem("cart")))
	}

	$('.owl-theme ').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }
	});

	$('.products-slider').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:2
	        }
	    }
	});

	// $(".cart_button").click(function() {
	// 	$(".over").addClass("active");
	// });
	
	$('section.cart').hide();
	$('section.empty_cart').hide();
	$('section.wishlist').hide();
	$('section.empty_wishlist').hide();
	$('section.user_sign_in').hide();
	$('section.user_login').hide();
	$('section.verification').hide();

	$(".over").click(function() {
		$(".over").removeClass("active");
	});
	$('.handburger').click(function () {
    	if($('.nav-links').hasClass("active")) {
	        var popup = $(".nav-links");
	        popup.hide(500);
	        $(".nav-links").removeClass("active");
	        // $(".over").removeClass("active");
	        // $(".over").hide(500);
    	}
    	else {
    		$(".nav-links").toggleClass("active");
	        $('.nav-links').show(500);
	    }
	});
	$('.cart_button').click(function () {
    	if($('section.cart').hasClass("active")) {
	        var popup = $("section.cart");
	        popup.hide(500);
	        $("section.cart").removeClass("active");
	        // $(".over").removeClass("active");
	        // $(".over").hide(500);
    	}
    	else {
    		$("section.cart").toggleClass("active");
	        $('section.cart').show(500);
			$(".over").addClass("active");
			
	    }
	});

	$('.wishlist_button').click(function () {
    	if($('section.wishlist').hasClass("active")) {
	        var popup = $("section.wishlist");
	        popup.hide(500);
	        $("section.wishlist").removeClass("active");
	        $("section.cart").removeClass("active");
    	}
    	else {
    		$("section.wishlist").toggleClass("active");
	        $('section.wishlist').show(500);
			$(".over").addClass("active");
			
	    }
	});

	$('.user_button').click(function () {
    	if($('section.user_sign_in').hasClass("active")) {
	        var popup = $("section.user_sign_in");
	        popup.hide(500);
	        $("section.user_sign_in").removeClass("active");
    	}
    	else {
    		$("section.user_sign_in").toggleClass("active");
			$('section.user_sign_in').show(500);
			
	    }
	});

	$('#login').click(function () {
    	if($('section.user_login').hasClass("active")) {
	        var popup = $("section.user_login");
	        popup.hide(500);
	        $("section.user_login").removeClass("active");

    	}
    	else {
    		$("section.user_login").toggleClass("active");
	        $('section.user_login').show(500);
	        $("section.user_sign_in").removeClass("active");
			$("section.user_sign_in").hide(500);
			
	    }
	});

	$('#signin').click(function () {
    	if($('section.user_sign_in').hasClass("active")) {
	        var popup = $("section.user_sign_in");
	        popup.hide(500);
	        $("section.user_sign_in").removeClass("active");
    	}
    	else {
    		$("section.user_login").removeClass("active");
	        $("section.user_login").hide(500);
    		$("section.user_sign_in").toggleClass("active");
			$('section.user_sign_in').show(500);
			

	    }
	});

	$('.verify').click(function () {
    	if($('section.verification').hasClass("active")) {
	        var popup = $("section.verification");
	        popup.hide(500);
	        $("section.verification").removeClass("active");
    	}
    	else {
    		$("section.verification").toggleClass("active");
	        $('section.verification').show(500);
	        $("section.user_login").removeClass("active");
			$('section.user_login').hide(500);
			
	    }
	});
	$(".back").click(function() {
		$("section.cart").removeClass("active");
		$(".over").removeClass("active");


	});

	$(".back_wishlist").click(function() {
		$("section.wishlist").removeClass("active")
		$(".over").removeClass("active");
	});


	$(document).mouseup(function (e) {
		if($('section.cart').hasClass("active")) {
		    var popup = $("section.cart");
		    if (!$('.cart_button').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
		        popup.hide(500);
		        $("section.cart").removeClass("active");
		 	}
	 	}
	 	else if($('section.wishlist').hasClass("active")) {
	 		var popup = $("section.wishlist");
		    if (!$('.wishlist_button').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
		        popup.hide(150);
		        $("section.wishlist").removeClass("active");
		 	}
	 	}
	 	else if($('section.user_sign_in').hasClass("active")) {
	 		var popup = $("section.user_sign_in");
		    if (!$('.user_button').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
		        popup.hide(150);
		        $("section.user_sign_in").removeClass("active");
		 	}
	 	}
	 	else if($('section.user_login').hasClass("active")) {
	 		var popup = $("section.user_login");
		    if (!$('#login').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
		        popup.hide(150);
		        $("section.user_login").removeClass("active");
		 	}
	 	}
	 	else if($('section.user_sign_in').hasClass("active")) {
	 		var popup = $("section.user_sign_in");
		    if (!$('#signin').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
		        popup.hide(150);
		        $("section.user_sign_in").removeClass("active");
		 	}
	 	}
	 	else if($('section.verification').hasClass("active")) {
	 		var popup = $("section.verification");
		    if (!$('.verify').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
		        popup.hide(500);
		        $("section.verification").removeClass("active");
		 	}
		 }
		 else if($('.nav-links').hasClass("active")) {
			var popup = $(".nav-links");
		   if (!$('.wishlist_button').is(e.target) && !popup.is(e.target) && popup.has(e.target).length == 0) {
			   popup.hide(500);
			   $(".nav-links").removeClass("active");
			}
		}
	});

	
	cart_qty=$("[data-totalqty]").data("totalqty");
	cartitem_quantity=0;
	quantity = 0;
	$(document).on('click','#add',(event)=>{
		event.preventDefault();
		cartitem_quantity = cartitem_quantity+1;
		quantity = cartitem_quantity;
		$("#qty").val(quantity);

	});
	$(document).on('click','#less',(event)=>{
		event.preventDefault();
		if (cartitem_quantity >=1) {
			cartitem_quantity = cartitem_quantity-1;
		}
		quantity = cartitem_quantity;
		$("#qty").val(quantity);

	});

	$("#add_to_cart").on("click",function(event){
		event.preventDefault();
		let isLoggedIn = localStorage.getItem("isLoggedIn");
		cart = {}
		console.log(isLoggedIn)
		if (isLoggedIn == "false"){
			cart = JSON.parse(localStorage.getItem("cart"))
			let $this = $(this);
			let pk = $this.data("id")
			if (quantity == 0) {
				cartitem_quantity = cartitem_quantity+1;
				quantity = cartitem_quantity;
				$("#qty").val(quantity);
			}

			// let quantity = $("[data-cartitemcount]").data("cartitemcount")
			console.log(quantity)
			cart_qty={}
			obj = {pk:pk,quantity:quantity};
			let updatedcarttotal = {...cart,obj};
			console.log(updatedcarttotal)
			localStorage.setItem("cart",JSON.stringify(updatedcarttotal))
			// if (cart_qty==0) {
			// 	obj = {pk:pk,quantity:quantity};
			// 	let updatedcarttotal = {...cart_qty,obj};
			// 	console.log(updatedcarttotal);
			// 	localStorage.setItem("cart_qty",JSON.stringify(updatedcarttotal));
			// 	cart_qty=cart_qty+cartitem_quantity;
			// 	$("#cart_qty").val(cart_qty)
			// }
		} else {
			console.log("hello")
		}
	});
});

