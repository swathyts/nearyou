from django.db import models
from main.models import BaseModel


class Category(BaseModel):
    image = models.ImageField(upload_to="products/categories")
    title = models.CharField(max_length=150)
    

    class Meta:
        db_table = 'products_category'
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
        ordering = ('title',)

    def __str__(self):
        return self.title


class Product(BaseModel):
    image = models.ImageField(upload_to="products/products")
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    category = models.ForeignKey("products.Category", on_delete=models.CASCADE)
    is_popular = models.BooleanField()

    def __str__(self):
        return self.name

